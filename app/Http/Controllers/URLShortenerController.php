<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ShortUrl;

class URLShortenerController extends Controller
{
    public function index()
    {
        $urls = ShortURL::all();

        return view('shorten', compact('urls'));
    }

    public function shorten(Request $request)
    {
        // Generate a unique short code for the URL
        $shortCode = ShortUrl::generateUniqueShortCode($request->input('long_url'));

        // Create a new ShortUrl record
        ShortUrl::create([
            'long_url' => $request->input('long_url'),
            'short_code' => $shortCode,
        ]);

        // Construct the short URL
        return redirect()->route('urls.index');
    }

    public function redirect($shortCode)
    {
        // Find the long URL corresponding to the short code
        $shortURL = ShortUrl::where('short_code', $shortCode)->firstOrFail();

        // Increment the click count
        $shortURL->incrementClickCount();

        // Redirect the user to the long URL
        return redirect($shortURL->long_url);
    }
}
