<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShortUrl extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public static function generateUniqueShortCode($longUrl)
    {
        $hashed = md5($longUrl);
        $shortCode = substr($hashed, 0, 6);
        
        if (self::where('short_code', $shortCode)->exists()) {
            return self::generateUniqueShortCode($longUrl);
        }

        return $shortCode;
    }

    public function incrementClickCount()
    {
        $this->click_count++;
        $this->save();
    }
}
