<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\URLShortenerController;

Route::get('/', [URLShortenerController::class, 'index'])->name('urls.index');
Route::post('/shorten', [URLShortenerController::class, 'shorten'])->name('shorten');
Route::get('/{shortCode}', [URLShortenerController::class, 'redirect'])->name('shorten.redirect');
