@extends('app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Shorten URL</div>

                <div class="card-body">
                    @if(count($urls) > 0)
                        <ul>
                            @foreach($urls as $url)
                                <li>
                                    <strong>Original URL:</strong> {{ $url->long_url }}<br>
                                    <strong>Short URL:</strong><a href="{{ route('shorten.redirect', ['shortCode' => $url->short_code]) }}" target="_blank">{{ route('shorten.redirect', ['shortCode' => $url->short_code]) }}</a>
                                    <br>
                                    <strong>Click Count:</strong> {{ $url->click_count }}
                                </li>
                                <br>
                            @endforeach
                        </ul>
                    @else
                        <p>No URLs found.</p>
                    @endif
                    @if(session('shortURL'))
                        <p><strong>Original URL:</strong></p>
                        <p>{{ session('originalURL') }}</p>

                        <p><strong>Short URL:</strong></p>
                        <p><a href="{{ session('shortURL') }}" target="_blank">{{ session('shortURL') }}</a></p>
                    @endif

                    <form action="{{ route('shorten') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="long_url">Long URL:</label>
                            <input type="url" class="form-control" id="long_url" name="long_url" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Shorten</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
